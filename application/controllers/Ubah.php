<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubah extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->model('Ubah_model');		
    }
	
	public function index()
	{
		$data['title'] = 'Ubah Password';
		
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('ubah/ubah_password',$data);
		$this->load->view('template/footer');
	}
	
	public function save_password()
 { 
 
		$data['title'] = 'Ubah Password';
		$this->load->model('Ubah_model');
		$this->form_validation->set_rules('new','New','required|alpha_numeric');
		$this->form_validation->set_rules('re_new', 'Retype New', 'required|matches[new]');
		$val = $this->form_validation->run();
		if($this->form_validation->run() == FALSE)
  {
	$this->session->set_flashdata('error','new password not same' );
	redirect('ubah');
  }else{
   $cek_old = $this->Ubah_model->cek_old();
   if ($cek_old == False){
    $this->session->set_flashdata('error','Old password not match!' );
	redirect('ubah');
   }else{
    $this->Ubah_model->save();
    $this->session->sess_destroy();
    $this->session->set_flashdata('error','Your password success to change, please relogin !' );
	   redirect();
	//$this->load->view('user/form_login',$data);
   }//end if valid_user
  }
 }

}
