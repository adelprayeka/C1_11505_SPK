<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_kriteria extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->model('sub_kriteria_model');
    }

	public function index()
	{

		$data['title'] = 'Daftar Sub Kriteria';
		$data['sub_kriteria'] = $this->sub_kriteria_model->data_sub_kriteria();

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('sub_kriteria/daftar',$data);
		$this->load->view('template/footer');
	}
	public function tambah()
	{
		$data['title'] = 'Tambah Sub_kriteria';

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('sub_kriteria/tambah',$data);
		$this->load->view('template/footer');
	}

	public function ubah($id)
	{
		$data['title'] = 'Ubah sub_kriteria';

		$data['sub_kriteria'] = $this->sub_kriteria_model->baca_sub_kriteria($id);

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('sub_kriteria/ubah',$data);
		$this->load->view('template/footer');
	}
	public function store()
	{
		# code...
		$kriteria = $_POST['kriteria'];
		$sub_kriteria = $_POST['sub_kriteria'];
		$nilai = $_POST['nilai'];

		$data = array(
			'kriteria' => $kriteria,
			'sub_kriteria' => $sub_kriteria,
			'nilai' => $nilai
		);

		$this->sub_kriteria_model->tambah_data($data);

		redirect('sub_kriteria');
	}

	public function store_ubah()
	{
		# code...
		$id = $_POST['id'];
		$kriteria = $_POST['kriteria'];
		$sub_kriteria = $_POST['sub_kriteria'];
		$nilai = $_POST['nilai'];

		$data = array(
			'kriteria' => $kriteria,
			'sub_kriteria' => $sub_kriteria,
			'nilai' => $nilai
		);

		$this->sub_kriteria_model->ubah_data($data,$id);

		redirect('sub_kriteria');
	}
	
	public function hapus($id)
	{
		
		$where = array('id' => $id);
		$this->sub_kriteria_model->hapus_sub_kriteria($id);
		redirect('sub_kriteria');		
	}
}
