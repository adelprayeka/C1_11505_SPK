<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->model('kriteria_model');
    }
	
	public function index()
	{

		$data['title'] = 'Daftar Kriteria';
		$data['kriteria'] = $this->kriteria_model->data_kriteria();

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('kriteria/daftar',$data);
		$this->load->view('template/footer');
	}
	
	public function tambah()
	{
		$data['title'] = 'Tambah kriteria';

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('kriteria/tambah',$data);
		$this->load->view('template/footer');
	}
	
	public function ubah($id)
	{
		$data['title'] = 'Ubah kriteria';

		$data['kriteria'] = $this->kriteria_model->baca_kriteria($id);

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('kriteria/ubah',$data);
		$this->load->view('template/footer');
	}
	
	public function store()
	{
		# code...
		$kriteria = $_POST['kriteria'];
		$tipe = $_POST['tipe'];
		

		$data = array(
			'kriteria' => $kriteria,
			'tipe' => $tipe,
		);

		$this->kriteria_model->tambah_data($data);

		redirect('kriteria');
	}
	
	public function store_ubah()
	{
		# code...
		$id = $_POST['id'];
		$kriteria = $_POST['kriteria'];
		$tipe = $_POST['tipe'];

		$data = array(
			'kriteria' => $kriteria,
			'tipe' => $tipe,
		);

		$this->kriteria_model->ubah_data($data,$id);

		redirect('kriteria');
	}
	
	public function hapus($id)
	{
		
		$where = array('id' => $id);
		$this->kriteria_model->hapus_kriteria($id);
		redirect('kriteria');		
	}
}
