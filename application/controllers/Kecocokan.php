<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecocokan extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->model('kecocokan_model');
    }

	public function index()
	{

		$data['title'] = 'Daftar Kecocokan';
		$data['kecocokan'] = $this->kecocokan_model;

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('kecocokan/daftar',$data);
		$this->load->view('template/footer');
	}
}
