<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		
	}
	
	public function index()
	{
		$data['title'] = 'User';
		
		$this->load->view('user/form_login',$data);
	}
	
	public function verify()
	{
		//ambil data dari form login
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		//konstruksi query where
		$where = array(
			'username' => $username,
			'password' => md5($password),
			);
		
		//cek ke database
		$cek = $this->user_model->cek_login($where)->num_rows();
		
		
		//jika pasangan username dan password ditemukan 
		if($cek ==1){
			//buat sesi baru
			$data_session = array(
				'username'=>$username,
				'status'=>'index'
				);
				
			$this->session->set_userdata($data_session);
			
			redirect(base_url('dashboard'));
		}
		else {
				$this->session->set_flashdata('error','Username atau password salah');
				redirect(base_url('user'));
			}
			
	}
	
	
	public function logout()
		{
			$this->session->sess_destroy();
			redirect(base_url('user/index'));
		}
	
}
