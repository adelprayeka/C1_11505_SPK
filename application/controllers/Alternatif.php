<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alternatif extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
		$this->load->helper(array('form', 'url'));
        $this->load->model('alternatif_model');		
    }

	public function index()
	{

		$data['title'] = 'Daftar alternatif';
		$data['alternatif'] = $this->alternatif_model->data_alternatif();

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('alternatif/daftar',$data);
		$this->load->view('template/footer');
	}
	public function tambah()
	{
		$data['title'] = 'Tambah alternatif';

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('alternatif/tambah',$data);
		$this->load->view('template/footer');
	}

	public function ubah($id)
	{
		$data['title'] = 'Ubah alternatif';

		$data['alternatif'] = $this->alternatif_model->baca_alternatif($id);

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('alternatif/ubah',$data);
		$this->load->view('template/footer');
	}
	
	public function upload()
	{
		$data['title'] = 'Tambah Gambar';

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('alternatif/form_upload',$data,array('error' => ' ' ));
		$this->load->view('template/footer');
	}
	
	
	public function store()
	{		
		# code...
		$alternatif = $_POST['alternatif'];
		$deskripsi = $_POST['deskripsi'];

		$data = array(
			'alternatif' => $alternatif,
			'deskripsi' => $deskripsi,
		);

		$this->alternatif_model->tambah_data($data);

		redirect('alternatif');
	}

	public function store_ubah()
	{
 
		$this->load->library('upload', $config);
		# code...
		$id = $_POST['id'];
		$alternatif = $_POST['alternatif'];
		$deskripsi = $_POST['deskripsi'];

		$data = array(
			'alternatif' => $alternatif,
			'deskripsi' => $deskripsi,
		);

		$this->alternatif_model->ubah_data($data,$id);

		redirect('alternatif');
	}
 
    public function aksi_upload() {
        $config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('gambar')){
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('template/header');
			$this->load->view('template/sidebar');
			$this->load->view('alternatif/form_upload',$error);
			$this->load->view('template/footer');;
		}else{
			$data = array('upload_data' => $this->upload->do_upload());
			$this->load->view('template/header');
			$this->load->view('template/sidebar');
			$this->load->view('alternatif/daftar',$data);
			$this->load->view('template/footer');
		}
		redirect('alternatif');
    }
	
	public function hapus($id)
	{
		
		$where = array('id' => $id);
		$this->alternatif_model->hapus_alternatif($id);
		redirect('alternatif');		
	}
}
