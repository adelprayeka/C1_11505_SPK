<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konsultasi extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->model('konsultasi_model');
    }
	
	public function index()
	{

		$data['title'] = 'Riwayat konsultasi';
		$data['konsultasi'] = $this->konsultasi_model->data_konsultasi();

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('konsultasi/daftar',$data);
		$this->load->view('template/footer');
	}
	
	public function tambah()
	{
		$data['title'] = 'Tambah konsultasi';

		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('konsultasi/tambah',$data);
		$this->load->view('template/footer');
	}
	
	public function store()
	{
		# code...
		$nama = $_POST['nama'];
		$waktu_konsultasi = date("Y-m-d H:i:s");

		$data = array(
			'nama' => $nama,
			'waktu_konsultasi' => $waktu_konsultasi,
		);

		$this->konsultasi_model->tambah_data($data);

		redirect('konsultasi');
	}
}
