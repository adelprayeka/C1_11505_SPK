<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria_model extends CI_Model {
	
	public function data_kriteria()
	{
		# code...
		$query = $this->db->get('kriteria');
		return $query->result();
	}
	 
	public function tambah_data($data)
	{
		# code...
		$this->db->insert('kriteria',$data);
		return TRUE;
	}
	
	public function baca_kriteria($id)
	{
		# code...
		$query = $this->db->get_where('kriteria',array(
			'id' => $id
		));
		return $query->row();
	}

	public function ubah_data($data,$id)
	{
		# code...
		$this->db->where('id', $id);
		$this->db->update('kriteria', $data);
		return TRUE;
	}

	function hapus_kriteria($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('kriteria');
	}	
}