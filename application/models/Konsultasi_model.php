<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konsultasi_model extends CI_Model {
	
	public function data_konsultasi()
	{
		# code...
		$query = $this->db->get('konsultasi');
		return $query->result();
	}
	 
	public function tambah_data($data)
	{
		# code...
		$this->db->insert('konsultasi',$data);
		return TRUE;
	}	 
}
