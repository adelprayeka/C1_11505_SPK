<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_kriteria_model extends CI_Model {
	 
	public function tambah_data($data)
	{
		# code...
		$this->db->insert('sub_kriteria',$data);
		return TRUE;
	}

	public function ubah_data($data,$id)
	{
		# code...
		$this->db->where('id', $id);
		$this->db->update('sub_kriteria', $data);
		return TRUE;
	}

	public function baca_sub_kriteria($id)
	{
		# code...
		$query = $this->db->get_where('sub_kriteria',array(
			'id' => $id
		));
		return $query->row();
	}

	public function data_sub_kriteria()
	{
		# code...
		$query = $this->db->get('sub_kriteria');
		return $query->result();
	}
	
	function hapus_sub_kriteria($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('sub_kriteria');
	}
}
