<div id="page-wrapper">
            <!-- isi kontentnya -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
					<div class="row">
						<div class="col-lg-3 col md-6">
						 <div class="panel panel-primary">
						  <div class="panel panel-heading">
						   <div class="row">
						   	<div class="col-xs-3">
							<i class="fa fa-stethoscope fa-5x">
														</i>
							</div>
							<div class="col-xs-9 text-right">
							 <div class="huge">71</div>
							 <div>Konsultasi</div>
							</div>							
							</div>
						  </div>
						  <a href="">
						   <div class="panel-footer">
						    <span class="pull-left">View Details</span>
							<span class="pull-right">
								<i class="fa fa-arrow-circle-right">								
								</i>
							</span>
						   <div class="clearfix">						  
						   </div>
						  </div>
						 </a>
						 </div>
						</div>
<!-- bates -->
						<div class="col-lg-3 col md-6">
						 <div class="panel panel-green">
						  <div class="panel panel-heading">
						   <div class="row">
						   	<div class="col-xs-3">
							<i class="fa fa-tags fa-5x">
														</i>
							</div>
							<div class="col-xs-9 text-right">
							 <div class="huge">5</div>
							 <div>Alternatif</div>
							</div>							
							</div>
						  </div>
						  <a href="">
						   <div class="panel-footer">
						    <span class="pull-left">View Details</span>
							<span class="pull-right">
								<i class="fa fa-arrow-circle-right">								
								</i>
							</span>
						   <div class="clearfix">						  
						   </div>
						  </div>
						 </a>
						 </div>
						</div>
<!-- bates -->
						<div class="col-lg-3 col md-6">
						 <div class="panel panel-yellow">
						  <div class="panel panel-heading">
						   <div class="row">
						   	<div class="col-xs-3">
							<i class="fa fa-sliders fa-5x">
														</i>
							</div>
							<div class="col-xs-9 text-right">
							 <div class="huge">3</div>
							 <div>Kriteria</div>
							</div>							
							</div>
						  </div>
						  <a href="">
						   <div class="panel-footer">
						    <span class="pull-left">View Details</span>
							<span class="pull-right">
								<i class="fa fa-arrow-circle-right">								
								</i>
							</span>
						   <div class="clearfix">						  
						   </div>
						  </div>
						 </a>
						 </div>
						</div>
<!-- bates -->
						<div class="col-lg-3 col md-6">
						 <div class="panel panel-red">
						  <div class="panel panel-heading">
						   <div class="row">
						   	<div class="col-xs-3">
							<i class="fa fa-sitemap fa-5x">
							</i>
							</div>
							<div class="col-xs-9 text-right">
							 <div class="huge">9</div>
							 <div>Sub Kriteria</div>
							</div>							
							</div>
						  </div>
						  <a href="">
						   <div class="panel-footer">
						    <span class="pull-left">View Details</span>
							<span class="pull-right">
								<i class="fa fa-arrow-circle-right">								
								</i>
							</span>
						   <div class="clearfix">						  
						   </div>
						  </div>
						 </a>
						 </div>
						</div>
<!-- bates -->
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Konsultasi Terbaru</div>
						<div class="panel-body">
						 <table class="table table-stripped table-bordered table-hover">
						  <thead>
						   <tr>
						    <th width="50px">#</th>
							<th>Nama</th>
							<th>Waktu</th>
							<th width="120px">&nbsp;</th>
						  </thead>
						  <tbody>
						   <tr></tr>
						   <tr></tr>
						   <tr></tr>
						   <tr></tr>
						  </tbody>
						 </table>
					</div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->