<div id="page-wrapper">
    <!-- isi kontentnya -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $title ?></h1>
        </div>
        <!-- /.col-lg-12 -->
        <a class="btn btn-primary" href="<?php echo base_url(); ?>konsultasi/tambah"> Konsultasi</a>
		<form method="get" action="" class="pull-right">
			<input name="q" class="search" value="">
			<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
		</form>
    </div>

    <br>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>nama</th>
                        <th>waktu konsultasi</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($konsultasi as $item): ?>
                        <tr>
                            <td>
                                <?php echo $item->id; ?>
                            </td>
                            <td>
                                <?php echo $item->nama; ?>
                            </td>
                            <td>
                                <?php echo $item->waktu_konsultasi; ?>
                            </td>
                            <td>
                                <a class="btn btn-warning" href="<?php echo base_url(); ?>konsultasi/detail/<?php echo $item->id; ?>">Detail</a>                       
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
        <!-- /#page-wrapper -->