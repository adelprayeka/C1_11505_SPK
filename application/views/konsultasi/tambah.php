<div id="page-wrapper">
            <!-- isi kontentnya -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $title ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="<?php echo base_url(); ?>konsultasi/store" method="POST">
                                        <div class="form-group">
                                            <label>Masukan Nama Anda</label>
                                            <input name="nama" class="form-control">
                                        </div>						
										<div class="alert alert-info" style="margin-top: 30px">Tentukan preferensi Anda terhadap 
										masing-masing kriteria berikut</div>
                                        <div class="form-group">
                                            <label>Harga</label>
                                            <select class="form-control" name="12" required="">
												<option value="5">Sangat Penting</option>
												<option value="4">Penting</option>
												<option value="3">Biasa</option>
												<option value="2">Tidak penting</option>
												<option value="1">Sangat Tidak penting</option>
											</select>
                                        </div>
                                        <div class="form-group">
                                            <label>Rasa</label>
                                            <select class="form-control" name="12" required="">
												<option value="5">Sangat Penting</option>
												<option value="4">Penting</option>
												<option value="3">Biasa</option>
												<option value="2">Tidak penting</option>
												<option value="1">Sangat Tidak penting</option>
											</select>
                                        </div>
										<div class="form-group">
                                            <label>Jarak</label>
                                            <select class="form-control" name="12" required="">
												<option value="5">Sangat Penting</option>
												<option value="4">Penting</option>
												<option value="3">Biasa</option>
												<option value="2">Tidak penting</option>
												<option value="1">Sangat Tidak penting</option>
											</select>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-info">
										<i class="fa fa-check"></i> Proses</button>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->