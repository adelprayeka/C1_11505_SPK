<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

	<title>Bootstrap Part 1 : Pengertian dan Cara Menggunakan Bootstrap</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
  </head>

  <body>
  <div class="container">
  <div class="row">
    <div class="col">
      
    </div>
    <div class="col-5">
	<br><br><br><br>
	<h2 class="from-signin-heading"><center>Admin Login</center></h2>
    <?php
       if ($this->session->flashdata('error')) {
            echo "<center><b>Username/password salah.</b></center>";
        }
    ?>
	<br><br><br><br>      
    </div>
    <div class="col">
     
    </div>
  </div>
  <div class="row">
    <div class="col">
      
    </div>
    <div class="col-5">
     <div class="container">

      <form class="form-signin" action="<?php echo base_url('user/verify'); ?>" method="post">
        <input type="text" name="username" class="form-control" placeholder="Username" required="" autofocus="">
        <input type="password" name="password" class="form-control" placeholder="Password" required="">
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container --> 
    </div>
    <div class="col">
      
    </div>
  </div>
</div>
</body>
</html>