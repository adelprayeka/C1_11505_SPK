<div id="page-wrapper">
            <!-- isi kontentnya -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $title ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo base_url(); ?>kriteria/store_ubah" method="POST">
                                        <input style="display: none;" type="text" name="id" value="<?php echo $kriteria->id ?>">
                                        <div class="form-group">
                                            <label>Kriteria</label>
                                            <input name="kriteria" class="form-control" value="<?php echo $kriteria->kriteria ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Tipe</label>
                                            <select class="form-control" name="tipe" required="">
												<option value="keuntungan">Keuntungan</option>
												<option value="biaya">Biaya</option>
											</select> 
                                        </div>                                         
                                        <button type="submit" class="btn btn-default">Simpan</button>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->