<div id="page-wrapper">
    <!-- isi kontentnya -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $title ?></h1>
        </div>
        <!-- /.col-lg-12 -->
        <a class="btn btn-primary" href="<?php echo base_url(); ?>kriteria/tambah"> Tambah Data Baru</a>
		<form method="get" action="" class="pull-right">
			<input name="q" class="search" value="">
			<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
		</form>
    </div>

    <br>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>kriteria</th>
                        <th>tipe</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($kriteria as $item): ?>
                        <tr>
                            <td>
                                <?php echo $item->id; ?>
                            </td>
                            <td>
                                <?php echo $item->kriteria; ?>
                            </td>
                            <td>
                                <?php echo $item->tipe; ?>
                            </td>
                            <td>
                                <a class="btn btn-warning" href="<?php echo base_url(); ?>kriteria/ubah/<?php echo $item->id; ?>">Ubah</a>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>kriteria/hapus/<?php echo $item->id; ?>"
								onclick="return confirm('Are you sure to delete this data?')">Hapus</a>                      
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
        <!-- /#page-wrapper -->