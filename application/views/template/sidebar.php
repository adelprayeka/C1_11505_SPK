<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">           
            <li>
                <a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>konsultasi"><i class="fa fa-history fa-fw"></i> Riwayat Konsultasi</a>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>kecocokan"><i class="fa fa-table fa-fw"></i> Rating Kecocokan</a>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>alternatif"><i class="fa fa-tags fa-fw"></i> Data Alternatif</a>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>kriteria"><i class="fa fa-sliders fa-fw"></i> Data Kriteria</a>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>sub_kriteria"><i class="fa fa-cubes fa-fw"></i> Data Sub Kriteria</a>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>ubah"><i class="fa fa-unlock fa-fw"></i> Ubah Password</a>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>user"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>