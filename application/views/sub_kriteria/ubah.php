<div id="page-wrapper">
            <!-- isi kontentnya -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $title ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo base_url(); ?>sub_kriteria/store_ubah" method="POST">
                                        <input style="display: none;" type="text" name="id" value="<?php echo $sub_kriteria->id ?>">
                                        <div class="form-group">
                                            <label>Kriteria</label>
											<select class="form-control" name="kriteria" required="" value="<?php echo $sub_kriteria->kriteria ?>">
												<option value="rasa">Rasa</option>
												<option value="biaya">Biaya</option>
												<option value="warung">Warung</option>
											</select>
                                        </div>
                                        <div class="form-group">
                                            <label>Sub Kriteria</label>
                                            <input name="sub_kriteria" class="form-control" value="<?php echo $sub_kriteria->sub_kriteria ?>"> 
                                        </div>
										<div class="form-group">
                                            <label>Nilai</label>
                                            <input type="number" name="nilai" class="form-control" value="<?php echo $sub_kriteria->nilai ?>">
                                        </div>
                                        <button type="submit" class="btn btn-default">Simpan</button>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->